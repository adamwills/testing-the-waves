$(function() {
	$('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top - $('.nav-fixed').height()
	        }, 1000);
	        return false;
	      }
	    }
	  });

	var mainTitlePos = $('.header h1').offset();

	$(window).scroll(function () {
	    if ($(window).scrollTop() > mainTitlePos.top) {
	        $('.the-nav').addClass('nav-fixed');
	        $('body').css('padding-top',$('.the-nav').height());
	        if ($(window).width() < 768 ) {$('.navbar-toggle').fadeIn();}
	    }
	    else {
	    	$('.the-nav').removeClass('nav-fixed');
	    	$('body').css('padding-top',0);
	    	if ($(window).width() < 768 ) {$('.navbar-toggle').fadeOut();}
	    }
	});
});