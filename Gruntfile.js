module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    autoprefixer: {
      no_dest: {
        src: 'app/css/styles.css'
      }
    },
    clean: {
      build: ['dist'],
      post: ['.tmp']
    },
    copy: {
      dist: {
        files: [
          {expand: true, flatten: true, src: ['app/*.{txt,xml}', 'app/404.html'], dest: 'dist/'},
        ]
      }
    },
    critical: {
      test: {
        options: {
          base: './',
          css: [
            'dist/css/styles.css'
          ],
          width: 320,
          height: 70
        },
        src: 'app/index.html',
        dest: '.tmp/index.html'
      }
    },
    cssmin: {
      dist: {
        files: [{
          expand: true,
          cwd: 'dist/css/',
          src: ['*.css', '!*.min.css'],
          dest: 'dist/css/',
          ext: '.min.css'
        }]
      }
    },
    htmlmin: {
      dist: {
        files: {
          'dist/index.html': '.tmp/index.html'
        }
      }
    },
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: 'app/img/',
          src: ['**/*.{png,jpg,svg,gif}'],
          dest: 'dist/img/'
        }]
      }
    },
    less: {
      dev: {
        files: {
          "app/css/styles.css": "app/less/app.less"
        }
      },
      dist: {
        files: {
         "dist/css/styles.css": "app/less/app.less"
        }
      }
    },
    modernizr: {
      dist: {
        "devFile": 'app/bower_components/modernizr/modernizr.js',
        "outputFile": "dist/js/modernizr-custom.js",
        "files": {
          "src": [
            'dist/**/**.*'
          ]
        }
      }
    },
    pagespeed: {
      options: {
        nokey: true,
        url: 'http://testingthewaves.com'
      },
      dist: {
        options: {
          url: 'http://testingthewaves.com',
          locale: 'en_GB',
          strategy: 'desktop',
          threshold: 80
        }
      }
    },
    processhtml: {
      dist: {
        files: {
          '.tmp/index.html': ['.tmp/index.html']
        }
      }
    },
    uglify: {
      dist: {
        files: {
          'dist/js/scripts.min.js': [
            'app/bower_components/jquery/dist/jquery.min.js',
            'app/bower_components/bootstrap/js/scrollspy.js',
            'app/bower_components/bootstrap/js/collapse.js',
            'app/js/plugins.js',
            'app/js/main.js'
          ]
        }
      }
    },
    watch: {
      options: {
        livereload: true,
      },
      css: {
        files: ['app/less/**/*.less'],
        tasks: ['less', 'autoprefixer'],
      },
    }
  });

  // load all grunt tasks matching the `grunt-*` pattern
  require('load-grunt-tasks')(grunt);


  // Default task(s).
  grunt.registerTask('default', ['less:dev', 'autoprefixer', 'modernizr']);
  grunt.registerTask('build', ['clean:build', 'less:dist', 'autoprefixer','cssmin', 'uglify', 'copy', 'critical', 'processhtml','modernizr','htmlmin','imagemin','clean:post' ]);

};